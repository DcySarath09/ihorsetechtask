package com.ihorsetech.task.service;

import retrofit2.Call;
import retrofit2.http.GET;

public interface GetPriceList {

    @GET("currentprice.json")
    Call<PriceList> getPriceListData();

}
