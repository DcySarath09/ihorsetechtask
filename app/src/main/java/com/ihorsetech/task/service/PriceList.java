package com.ihorsetech.task.service;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class PriceList {

    @Expose
    @SerializedName("bpi")
    public Bpi bpi;
    @Expose
    @SerializedName("chartName")
    public String chartName;
    @Expose
    @SerializedName("disclaimer")
    public String disclaimer;
    @Expose
    @SerializedName("time")
    public Time time;

    public static class Bpi {
        @Expose
        @SerializedName("EUR")
        public EUR EUR;
        @Expose
        @SerializedName("GBP")
        public GBP GBP;
        @Expose
        @SerializedName("USD")
        public USD USD;
    }

    public static class EUR {
        @Expose
        @SerializedName("rate_float")
        public double rate_float;
        @Expose
        @SerializedName("description")
        public String description;
        @Expose
        @SerializedName("rate")
        public String rate;
        @Expose
        @SerializedName("symbol")
        public String symbol;
        @Expose
        @SerializedName("code")
        public String code;
    }

    public static class GBP {
        @Expose
        @SerializedName("rate_float")
        public double rate_float;
        @Expose
        @SerializedName("description")
        public String description;
        @Expose
        @SerializedName("rate")
        public String rate;
        @Expose
        @SerializedName("symbol")
        public String symbol;
        @Expose
        @SerializedName("code")
        public String code;
    }

    public static class USD {
        @Expose
        @SerializedName("rate_float")
        public double rate_float;
        @Expose
        @SerializedName("description")
        public String description;
        @Expose
        @SerializedName("rate")
        public String rate;
        @Expose
        @SerializedName("symbol")
        public String symbol;
        @Expose
        @SerializedName("code")
        public String code;
    }

    public static class Time {
        @Expose
        @SerializedName("updateduk")
        public String updateduk;
        @Expose
        @SerializedName("updatedISO")
        public String updatedISO;
        @Expose
        @SerializedName("updated")
        public String updated;
    }
}
