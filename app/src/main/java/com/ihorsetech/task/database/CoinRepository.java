package com.ihorsetech.task.database;

import android.content.Context;
import android.os.AsyncTask;

import androidx.lifecycle.LiveData;
import androidx.room.Room;

import com.ihorsetech.task.uitls.AppUtils;

import java.util.List;

public class CoinRepository {

    private String DB_NAME = "db_task";

    private CoinListDatabase coinListDatabase;
    public CoinRepository(Context context) {
        coinListDatabase = Room.databaseBuilder(context, CoinListDatabase.class, DB_NAME).build();
    }

    public void insertTask(String title, String coinModels) {

        insertTask(title,coinModels,null);
    }

    public void insertTask(String title,
                           String coinModels,String wsf) {

        CoinList coinList = new CoinList();
        coinList.setTitle(title);
        coinList.setCreatedAt(AppUtils.getCurrentDateTime());
        coinList.setCoinModel(coinModels);
        insertTask(coinList);
    }

    public void insertTask(final CoinList note) {
        new AsyncTask<Void, Void, Void>() {
            @Override
            protected Void doInBackground(Void... voids) {
                coinListDatabase.daoAccess().insertTask(note);
                return null;
            }
        }.execute();
    }

    public void updateTask(final CoinList note) {

        new AsyncTask<Void, Void, Void>() {
            @Override
            protected Void doInBackground(Void... voids) {
                coinListDatabase.daoAccess().updateTask(note);
                return null;
            }
        }.execute();
    }

    public void deleteTask(final int id) {
        final LiveData<CoinList> task = getTask(id);
        if(task != null) {
            new AsyncTask<Void, Void, Void>() {
                @Override
                protected Void doInBackground(Void... voids) {
                    coinListDatabase.daoAccess().deleteTask(task.getValue());
                    return null;
                }
            }.execute();
        }
    }

    public void deleteTask(final CoinList note) {
        new AsyncTask<Void, Void, Void>() {
            @Override
            protected Void doInBackground(Void... voids) {
                coinListDatabase.daoAccess().deleteTask(note);
                return null;
            }
        }.execute();
    }

    public LiveData<CoinList> getTask(int id) {
        return coinListDatabase.daoAccess().getTask(id);
    }

    public LiveData<List<CoinList>> getTasks() {
        return coinListDatabase.daoAccess().fetchAllTasks();
    }
}
