package com.ihorsetech.task.database;


import androidx.lifecycle.LiveData;
import androidx.room.Dao;
import androidx.room.Delete;
import androidx.room.Insert;
import androidx.room.Query;
import androidx.room.Update;

import java.util.List;

@Dao
public interface DaoAccess {

    @Insert
    Long insertTask(CoinList coinList);


    @Query("SELECT * FROM CoinList ORDER BY created_at desc")
    LiveData<List<CoinList>> fetchAllTasks();


    @Query("SELECT * FROM CoinList WHERE id =:taskId")
    LiveData<CoinList> getTask(int taskId);


    @Update
    void updateTask(CoinList coinList);


    @Delete
    void deleteTask(CoinList coinList);
}
