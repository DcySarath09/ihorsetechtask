package com.ihorsetech.task.database;

import androidx.room.Database;
import androidx.room.RoomDatabase;

@Database(entities = {CoinList.class}, version = 1, exportSchema = false)
public abstract class CoinListDatabase extends RoomDatabase {
    public abstract DaoAccess daoAccess();
}
