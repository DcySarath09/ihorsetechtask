package com.ihorsetech.task.database;

import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.PrimaryKey;
import androidx.room.TypeConverters;

import com.ihorsetech.task.uitls.TimestampConverter;

import java.io.Serializable;
import java.util.Date;

@Entity
public class CoinList implements Serializable {

    @PrimaryKey(autoGenerate = true)
    private int id;

    String title;
    String coinModel;
    @ColumnInfo(name = "created_at")
    @TypeConverters({TimestampConverter.class})
    private Date createdAt;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getCoinModel() {
        return coinModel;
    }

    public void setCoinModel(String coinModel) {
        this.coinModel = coinModel;
    }

    public Date getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(Date createdAt) {
        this.createdAt = createdAt;
    }
}
