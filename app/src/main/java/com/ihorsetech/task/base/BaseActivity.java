package com.ihorsetech.task.base;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Build;
import android.os.Bundle;
import android.util.Patterns;
import android.view.Gravity;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.LinearLayout;
import android.widget.Toast;
import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AppCompatActivity;
import com.ihorsetech.task.R;
import java.util.regex.Pattern;

/**
 * Created by admin on 14-03-2017.
 */

public class BaseActivity extends AppCompatActivity {

    //Create objects
    ActionBar actionBar;
    Dialog progressdialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        actionBar = getSupportActionBar();
    }

    /******Hide ActionBar*******/
    public void hideActionBar(){
        actionBar.hide();
    }

    /***** SET ActionBar BackArrow*****/
    public void showBackArrow(){
        actionBar.setDisplayShowHomeEnabled(true);
        actionBar.setDisplayShowTitleEnabled(true);
        actionBar.setDisplayHomeAsUpEnabled(true);
    }

    /***** SET ActionBar Title*****/
    public void setTitle(String actionBarName){
        actionBar.setTitle(actionBarName);
    }

    /******Transparent Toolbar*******/
    public void transparentToolbar() {
        if (Build.VERSION.SDK_INT >= 19 && Build.VERSION.SDK_INT < 21) {
            setWindowFlag(this, WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS, true);
        }
        if (Build.VERSION.SDK_INT >= 19) {
            getWindow().getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_LAYOUT_STABLE | View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN);
        }
        if (Build.VERSION.SDK_INT >= 21) {
            setWindowFlag(this, WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS, false);
            getWindow().setStatusBarColor(Color.TRANSPARENT);
        }
    }

    /******Hide System UI*******/
    public void hideSystemUI() {
        if (Build.VERSION.SDK_INT >= 21) {
            setWindowFlag(this, WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS, false);
            getWindow().setStatusBarColor(Color.TRANSPARENT);
            getWindow().getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_LIGHT_STATUS_BAR);
        }
    }

    /******Set Window Flag*******/
    public static void setWindowFlag(Activity activity, final int bits, boolean on) {
        Window win = activity.getWindow();
        WindowManager.LayoutParams winParams = win.getAttributes();
        if (on) {
            winParams.flags |= bits;
        } else {
            winParams.flags &= ~bits;
        }
        win.setAttributes(winParams);
    }

    /******Toast Method*******/
    public void toast(String message) {
        Toast toast = Toast.makeText(this, message, Toast.LENGTH_SHORT);
        toast.setGravity(Gravity.CENTER_VERTICAL | Gravity.CENTER_HORIZONTAL, 0, 0);
        toast.show();
    }

    /******No Internet Alert dialog********/
    public void noInternetAlertDialog() {
        AlertDialog.Builder alertDialog = new AlertDialog.Builder(this);
        alertDialog.setTitle(getResources().getString(R.string.Internet_Alert));
        alertDialog.setPositiveButton(getResources().getString(R.string.OK), new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                dialog.cancel();
            }
        });
        alertDialog.show();
    }

    /*********Check valid email Method********/
    public boolean validEmail(String email) {
        Pattern pattern = Patterns.EMAIL_ADDRESS;
        return pattern.matcher(email).matches();
    }


    /**********Check Internet Connection Method*****/
    public boolean isConnectingToInternet() {
        ConnectivityManager connectivity = (ConnectivityManager) this.getSystemService(Context.CONNECTIVITY_SERVICE);
        if (connectivity != null) {
            NetworkInfo[] info = connectivity.getAllNetworkInfo();
            if (info != null)
                for (int i = 0; i < info.length; i++)

                    if (info[i].getState() == NetworkInfo.State.CONNECTED) {
                        return true;
                    }

        }
        return false;
    }

    /*******Show progressbar************/
    public void showProgressDialog() {
        try{

            if (progressdialog == null) {
                progressdialog = new Dialog(this);
                progressdialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
                progressdialog.setContentView(R.layout.custom_progressbar);
                progressdialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
                progressdialog.getWindow().setLayout(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT);
            }

            progressdialog.setCancelable(false);
            progressdialog.show();

        }catch (Exception e){e.printStackTrace();}

    }

    /*******Hide progressbar************/
    public void hideProgressDialog() {
        try {
            if (progressdialog.isShowing()) {
                progressdialog.dismiss();
            }
        } catch (Exception e) { }
    }

    /*******System back event************/
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        switch (id) {
            case android.R.id.home:
                finish();
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    /****Get time conversion*****/
    public String getStartTime(int yearStart, int monthStart, int dayStart){

        String f_year = String.valueOf(yearStart);
        String f_month = String.valueOf(monthStart);
        String f_date = String.valueOf(dayStart);
        if(f_month.length()!=2){ f_month = "0"+f_month; }
        if(f_date.length()!=2){ f_date = "0"+f_date; }
        String F_DATE = f_year+"-"+f_month+"-"+f_date;
        return F_DATE;
    }

    public String getEndTime(int yearEnd, int monthEnd, int dayEnd){

        String t_year = String.valueOf(yearEnd);
        String t_month = String.valueOf(monthEnd);
        String t_date = String.valueOf(dayEnd);
        if(t_month.length()!=2){ t_month = "0"+t_month; }
        if(t_date.length()!=2){ t_date = "0"+t_date; }
        String T_DATE = t_year+"-"+t_month+"-"+t_date;
        return T_DATE;
    }
}
