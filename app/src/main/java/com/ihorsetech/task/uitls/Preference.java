package com.ihorsetech.task.uitls;

import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;

public class Preference {

    Activity activity;

    SharedPreferences preferences;
    SharedPreferences.Editor editor;

    SharedPreferences locationPreferences;
    SharedPreferences.Editor locationEditor;

    public static Preference loginSession = null;

    //Constructor
    public Preference(Activity activity) {
        super();
        this.activity = activity;
        try {
            preferences = this.activity.getSharedPreferences("login", Context.MODE_PRIVATE);
            editor = preferences.edit();

            locationPreferences = this.activity.getSharedPreferences("login", Context.MODE_PRIVATE);
            locationEditor = locationPreferences.edit();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static Preference getInstance(Activity activity) {
        if (loginSession == null){
            loginSession = new Preference(activity);
        }
        return loginSession;
    }

    //save login state
    public void saveLogin(boolean login)
    {
        editor.putBoolean("login", login);
        editor.commit();
    }

    //login check
    public boolean isLoggedin()
    {
        return preferences.getBoolean("login",false);
    }

}
