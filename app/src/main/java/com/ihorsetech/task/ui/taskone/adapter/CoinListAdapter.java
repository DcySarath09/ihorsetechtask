package com.ihorsetech.task.ui.taskone.adapter;

import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.ihorsetech.task.R;
import com.ihorsetech.task.database.CoinList;

import org.json.JSONArray;
import org.json.JSONException;

import java.util.List;

public class CoinListAdapter extends RecyclerView.Adapter<CoinListAdapter.CustomViewHolder> {

    private List<CoinList> coinLists;
    public CoinListAdapter(List<CoinList> coinLists) {
        this.coinLists = coinLists;
    }

    @NonNull
    @Override
    public CustomViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.coin_list_item, null);
        CustomViewHolder viewHolder = new CustomViewHolder(view);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull CustomViewHolder holder, int position) {
        CoinList coinList = getItem(position);
        holder.codeTxt.setText(coinList.getTitle());

        try{
        JSONArray arr = new JSONArray(coinList.getCoinModel());
        for(int i = 0; i < arr.length(); i++){
            holder.symbolTxtview.setText(Html.fromHtml(arr.getJSONObject(i).getString("symbol")));
            holder.rateTxtview.setText(arr.getJSONObject(i).getString("rate"));
            holder.descriptionTxtview.setText(arr.getJSONObject(i).getString("description"));

        }
        }catch (JSONException e){}
    }

    @Override
    public int getItemCount() {
        return coinLists.size();
    }

    public CoinList getItem(int position) {
        return coinLists.get(position);
    }

    protected class CustomViewHolder extends RecyclerView.ViewHolder {

        private TextView codeTxt, symbolTxtview,rateTxtview,descriptionTxtview;
        public CustomViewHolder(View itemView) {
            super(itemView);

            codeTxt = itemView.findViewById(R.id.codeTxt);
            symbolTxtview = itemView.findViewById(R.id.symbolTxtview);
            rateTxtview = itemView.findViewById(R.id.rateTxtview);
            descriptionTxtview = itemView.findViewById(R.id.descriptionTxtview);
        }
    }
}
