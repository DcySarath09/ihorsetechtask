package com.ihorsetech.task.ui.taskone.adapter;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.ihorsetech.task.R;
import com.ihorsetech.task.ui.taskone.DatePriceModel;

import java.util.List;

public class DatePriceListAdapter extends RecyclerView.Adapter<DatePriceListAdapter.CustomViewHolder> {

    private List<DatePriceModel> datePriceList;
    public DatePriceListAdapter(List<DatePriceModel> datePriceList) {
        this.datePriceList = datePriceList;
    }

    @NonNull
    @Override
    public CustomViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.date_list_item, null);
        CustomViewHolder viewHolder = new CustomViewHolder(view);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull CustomViewHolder holder, int position) {

        holder.dateTxt.setText(datePriceList.get(position).getData());
        holder.priceTxt.setText(datePriceList.get(position).getAmount());

    }

    @Override
    public int getItemCount() {
        return datePriceList.size();
    }

    public DatePriceModel getItem(int position) {
        return datePriceList.get(position);
    }

    protected class CustomViewHolder extends RecyclerView.ViewHolder {

        private TextView dateTxt, priceTxt;
        public CustomViewHolder(View itemView) {
            super(itemView);
            dateTxt = itemView.findViewById(R.id.dateTxt);
            priceTxt = itemView.findViewById(R.id.priceTxt);
        }
    }
}

