package com.ihorsetech.task.ui.taskone;

import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.View;

import androidx.annotation.Nullable;
import androidx.databinding.DataBindingUtil;
import androidx.lifecycle.Observer;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.ihorsetech.task.R;
import com.ihorsetech.task.base.BaseActivity;
import com.ihorsetech.task.database.CoinList;
import com.ihorsetech.task.database.CoinRepository;
import com.ihorsetech.task.databinding.CurrentpriceScreenBinding;
import com.ihorsetech.task.service.GetPriceList;
import com.ihorsetech.task.service.PriceList;
import com.ihorsetech.task.service.RetrofitInstance;
import com.ihorsetech.task.ui.taskone.adapter.CoinListAdapter;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class CurrencyRateScreen extends BaseActivity {

    //Binding views
    CurrentpriceScreenBinding binding;
    private CoinListAdapter coinListAdapter;

    //DataBase
    private CoinRepository coinRepository;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        initView();
        showBackArrow();
        setTitle("Currency Rate");

        //Initialize objects
        coinRepository = new CoinRepository(getApplicationContext());

        //Get database values
        updateTaskList();

    }

    private void initView() {
        binding = DataBindingUtil.setContentView(this, R.layout.currentprice_screen);
        binding.pricelist.setLayoutManager(new LinearLayoutManager(this, RecyclerView.VERTICAL,false));
    }

    private void updateTaskList() {

        coinRepository.getTasks().observe(this, new Observer<List<CoinList>>() {
            @Override
            public void onChanged(@Nullable List<CoinList> coinList) {
                if(coinList.size()>0){
                    binding.pricelist.setVisibility(View.VISIBLE);
                    if(coinListAdapter==null){
                        toast("Get data from Database");
                        coinListAdapter = new CoinListAdapter(coinList);
                        binding.pricelist.setAdapter(coinListAdapter);
                    }
                }else{
                    if(isConnectingToInternet()){
                        callAPIServie();
                    }else{
                        noInternetAlertDialog();
                    }

                }
            }
        });
    }

    private void callAPIServie() {

        /** Create handle for the RetrofitInstance interface*/
        GetPriceList service = RetrofitInstance.getRetrofitInstance().create(GetPriceList.class);
        Call<PriceList> call = service.getPriceListData();
        Log.wtf("URL Called", call.request().url() + "");
        showProgressDialog();
        call.enqueue(new Callback<PriceList>() {
            @Override
            public void onResponse(Call<PriceList> call, Response<PriceList> response) {
                hideProgressDialog();
                if(response!=null){
                    showProgressDialog();
                    storeValuesToDataBase(response);
                    toast("Get data from api");
                }
            }
            @Override
            public void onFailure(Call<PriceList> call, Throwable t) {
                hideProgressDialog();
                toast("Something went wrong...");
            }
        });
    }

    private void storeValuesToDataBase(Response<PriceList> response) {

        try {

            //USD
            JSONArray jsonArray = new JSONArray();
            JSONObject jsonObject = new JSONObject();
            jsonObject.put("code",response.body().bpi.USD.code);
            jsonObject.put("symbol",response.body().bpi.USD.symbol);
            jsonObject.put("rate",response.body().bpi.USD.rate);
            jsonObject.put("description",response.body().bpi.USD.description);
            jsonObject.put("rate_float",response.body().bpi.USD.rate_float);
            jsonArray.put(jsonObject);
            coinRepository.insertTask(response.body().bpi.USD.code,jsonArray.toString().trim());

            //GBP
            JSONArray jsonArrayGBP = new JSONArray();
            JSONObject jsonObjectGBP = new JSONObject();
            jsonObject.put("code",response.body().bpi.GBP.code);
            jsonObjectGBP.put("symbol",response.body().bpi.GBP.symbol);
            jsonObjectGBP.put("rate",response.body().bpi.GBP.rate);
            jsonObjectGBP.put("description",response.body().bpi.GBP.description);
            jsonObjectGBP.put("rate_float",response.body().bpi.GBP.rate_float);
            jsonArrayGBP.put(jsonObjectGBP);
            coinRepository.insertTask(response.body().bpi.GBP.code,jsonArrayGBP.toString().trim());

            //EUR
            JSONArray jsonArrayEUR = new JSONArray();
            JSONObject jsonObjectEUR = new JSONObject();
            jsonObject.put("code",response.body().bpi.EUR.code);
            jsonObjectEUR.put("symbol",response.body().bpi.EUR.symbol);
            jsonObjectEUR.put("rate",response.body().bpi.EUR.rate);
            jsonObjectEUR.put("description",response.body().bpi.EUR.description);
            jsonObjectEUR.put("rate_float",response.body().bpi.EUR.rate_float);
            jsonArrayEUR.put(jsonObjectEUR);
            coinRepository.insertTask(response.body().bpi.EUR.code,jsonArrayEUR.toString().trim());

            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {
                    hideProgressDialog();
                    updateTaskList();

                }
            },1000);


        } catch (JSONException e) {
            e.printStackTrace();
        }
    }
}
