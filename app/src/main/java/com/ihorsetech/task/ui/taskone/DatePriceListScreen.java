package com.ihorsetech.task.ui.taskone;

import android.os.Bundle;
import android.util.Log;
import android.view.View;

import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.ihorsetech.task.R;
import com.ihorsetech.task.base.BaseActivity;
import com.ihorsetech.task.base.MyApplication;
import com.ihorsetech.task.databinding.DatepricelistScreenBinding;
import com.ihorsetech.task.ui.taskone.adapter.DatePriceListAdapter;
import com.leavjenn.smoothdaterangepicker.date.SmoothDateRangePickerFragment;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

public class DatePriceListScreen extends BaseActivity{

    DatepricelistScreenBinding binding;
    private DatePriceListAdapter datePriceListAdapter;

    //Service
    private String tag_datepricelist_req = "DatePricelistRequest";

    //String objects
    String F_DATE = "";
    String T_DATE = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        initView();
        showBackArrow();
        setTitle("Price List");
        //Get database values
        openDatePicker();

        binding.resetBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                openDatePicker();
            }
        });
    }

    private void initView() {
        binding = DataBindingUtil.setContentView(this, R.layout.datepricelist_screen);
        binding.datelist.setLayoutManager(new LinearLayoutManager(this, RecyclerView.VERTICAL,false));
    }

    private void callAPIServie(String fdate, String tdate) {

        showProgressDialog();
        String URL = "https://api.coindesk.com/v1/bpi/historical/close.json?start="+fdate+"&end="+tdate;
        Log.e("URL",URL);
        final Map<String, String> params = new HashMap<>();
        StringRequest postRequest = new StringRequest(Request.Method.GET,URL,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        try {
                            hideProgressDialog();
                            if (response != null) {
                                JSONObject jsonObject = new JSONObject(response);
                                String dateList = jsonObject.getString("bpi").trim();

                                ArrayList<DatePriceModel> datePriceList = new ArrayList<DatePriceModel>();
                                JSONObject jsonObject1 = new JSONObject(dateList);
                                Iterator<String> iter = jsonObject1.keys();
                                while (iter.hasNext()) {
                                    DatePriceModel datePriceModel = new DatePriceModel();
                                    String key = iter.next();
                                    Log.e("KEY",key);
                                    datePriceModel.setData(key);
                                    try {
                                        Object value = jsonObject1.get(key);
                                        Log.e("value",""+value);
                                        datePriceModel.setAmount(String.valueOf(value));
                                        datePriceList.add(datePriceModel);
                                    } catch (JSONException e) {
                                        // Something went wrong!
                                    }
                                }

                                if(datePriceList.size()>0){
                                    binding.datelist.setVisibility(View.VISIBLE);
                                        datePriceListAdapter = new DatePriceListAdapter(datePriceList);
                                        binding.datelist.setAdapter(datePriceListAdapter);
                                        datePriceListAdapter.notifyDataSetChanged();

                                }else{
                                    if(isConnectingToInternet()){
                                        callAPIServie(F_DATE,T_DATE);
                                    }else{
                                        noInternetAlertDialog();
                                    }

                                }
                            }

                        } catch (Exception e) {
                            e.printStackTrace();
                            hideProgressDialog();
                        }

                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        error.printStackTrace();
                        toast(error.toString());
                    }
                }
        ) {

            @Override
            protected Map<String, String> getParams() {
                Map<String, String> param2 = new HashMap<String, String>();
                param2 = params;
                return param2;

            }
        };
        // Adding request to request queue
        MyApplication.getInstance().addToRequestQueue(postRequest, tag_datepricelist_req);

    }

    /*Data Picker*/
    private void openDatePicker() {
        SmoothDateRangePickerFragment smoothDateRangePickerFragment = SmoothDateRangePickerFragment.newInstance(
                new SmoothDateRangePickerFragment.OnDateRangeSetListener() {
                    @Override
                    public void onDateRangeSet(SmoothDateRangePickerFragment view,
                                               int yearStart, int monthStart,
                                               int dayStart, int yearEnd,
                                               int monthEnd, int dayEnd) {


                        F_DATE = getStartTime(yearStart,monthStart,dayStart);
                        T_DATE = getEndTime(yearEnd,monthEnd,dayEnd);
                        binding.dateRangeTxt.setText(F_DATE+" to "+T_DATE);
                        if(isConnectingToInternet()){
                            callAPIServie(F_DATE,T_DATE);
                        }else{
                            noInternetAlertDialog();
                        }
                    }
                });

        smoothDateRangePickerFragment.show(getFragmentManager(), "smoothDateRangePicker");
        smoothDateRangePickerFragment.setCancelable(false);

    }

}
