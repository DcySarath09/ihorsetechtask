package com.ihorsetech.task.ui.taskthree;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.text.Html;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.Spinner;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;

import com.ihorsetech.task.R;
import com.ihorsetech.task.database.CoinList;

import org.json.JSONArray;
import org.json.JSONException;
import org.w3c.dom.Text;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.Locale;

public class UserListAdapter extends RecyclerView.Adapter<UserListAdapter.CustomViewHolder> {

    private List<UserModel> userModelListOriginal;
    private List<UserModel> userModelListDummy;
    TaskthreeScreen taskthreeScreen;
    Activity activity;
    Dialog editUserDialogue;
    String selectedTab;

    public UserListAdapter(List<UserModel> userModelList,TaskthreeScreen taskthreeScreen,String selectedTab) {
        this.userModelListOriginal = userModelList;
        this.userModelListDummy = new ArrayList<>();
        this.userModelListDummy.addAll(userModelListOriginal);
        this.taskthreeScreen = taskthreeScreen;
        this.selectedTab = selectedTab;
    }

    @NonNull
    @Override
    public CustomViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.user_list_item, null);
        CustomViewHolder viewHolder = new CustomViewHolder(view);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull CustomViewHolder holder, final int position) {
        UserModel userModel = getItem(position);

        if(userModel.getGender().equalsIgnoreCase(selectedTab)){
            holder.cardView.setVisibility(View.VISIBLE);
        }  else{
            holder.cardView.setVisibility(View.GONE);
        }

        holder.indexTextView.setText(String.valueOf(position));
        holder.firstNameTextView.setText(userModel.getFirstName());
        holder.lastNameTextview.setText(userModel.getLastName());
        holder.dobTxtview.setText(userModel.getDob());

        holder.deleteButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                AlertDialog.Builder alertDialog = new AlertDialog.Builder(taskthreeScreen);
                alertDialog.setTitle("Are you sure");

                alertDialog.setPositiveButton("YES", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {

                        taskthreeScreen.callDelete(position);
                        dialog.cancel();
                    }
                });

                alertDialog.setNegativeButton("NO", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        dialogInterface.dismiss();
                    }
                });

                alertDialog.show();
            }
        });

        holder.editButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if (editUserDialogue == null) {
                    editUserDialogue = new Dialog(taskthreeScreen);
                    editUserDialogue.requestWindowFeature(Window.FEATURE_NO_TITLE);
                    editUserDialogue.setContentView(R.layout.dialog_for_adduser);
                    editUserDialogue.getWindow().setBackgroundDrawable(new ColorDrawable(Color.WHITE));
                    editUserDialogue.getWindow().setLayout(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.MATCH_PARENT);
                }

                final TextView titleTxt = (TextView)editUserDialogue.findViewById(R.id.titleTxt);
                final EditText nameEditText = (EditText)editUserDialogue.findViewById(R.id.nameEditText);
                final EditText lastNameEditText = (EditText)editUserDialogue.findViewById(R.id.lastNameEditText);
                final EditText dobEditText = (EditText)editUserDialogue.findViewById(R.id.dobEditText);
                final RadioButton maleBtn = (RadioButton)editUserDialogue.findViewById(R.id.maleBtn);
                RadioButton femaleBtn = (RadioButton)editUserDialogue.findViewById(R.id.femaleBtn);
                Button submitBtn = (Button)editUserDialogue.findViewById(R.id.submitBtn);

                titleTxt.setText(R.string.UpdateUser);
                //setSpinner values
                dobEditText.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        getDoBfromPopUp(dobEditText);
                    }
                });


                nameEditText.setText(userModelListOriginal.get(position).getFirstName());
                lastNameEditText.setText(userModelListOriginal.get(position).getLastName());
                dobEditText.setText(userModelListOriginal.get(position).getDob());

                if(userModelListOriginal.get(position).getGender().equalsIgnoreCase("MALE")){
                    maleBtn.setChecked(true);
                }else{
                    femaleBtn.setChecked(true);
                }

                submitBtn.setText("Update");

                submitBtn.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {

                        String fName = nameEditText.getText().toString().trim();
                        String lName = lastNameEditText.getText().toString().trim();
                        String dob = dobEditText.getText().toString().trim();

                        if(fName.isEmpty()){
                            nameEditText.setError("Please enter a First Name");
                        }else if(lName.isEmpty()){
                            lastNameEditText.setError("Please enter a Last Name");
                        }else{

                            UserModel userModel = new UserModel();
                            userModel.setFirstName(fName);
                            userModel.setLastName(lName);
                            userModel.setDob(dob);

                            if(maleBtn.isChecked()){
                                userModel.setGender("Male");
                            }else{
                                userModel.setGender("Female");
                            }
                            taskthreeScreen.updateUser(position,userModel);
                            editUserDialogue.dismiss();
                        }
                    }
                });

                editUserDialogue.setCancelable(true);
                editUserDialogue.show();
            }
        });
    }

    @Override
    public int getItemCount() {
        return userModelListOriginal.size();
    }

    public UserModel getItem(int position) {
        return userModelListOriginal.get(position);
    }

    protected class CustomViewHolder extends RecyclerView.ViewHolder {

        private TextView firstNameTextView, lastNameTextview,dobTxtview,indexTextView;
        private Button deleteButton,editButton;
        private CardView cardView;
        public CustomViewHolder(View itemView) {
            super(itemView);

            indexTextView = itemView.findViewById(R.id.indexTextView);
            firstNameTextView = itemView.findViewById(R.id.firstNameTextView);
            lastNameTextview = itemView.findViewById(R.id.lastNameTextview);
            dobTxtview = itemView.findViewById(R.id.dobTxtview);
            deleteButton = itemView.findViewById(R.id.deleteButton);
            editButton = itemView.findViewById(R.id.editButton);
            cardView = itemView.findViewById(R.id.cardView);
        }
    }

    /*DOB PopUp*/
    private void getDoBfromPopUp(final EditText dobEditText) {

        final String returnValue = "";
        AlertDialog.Builder builderSingle = new AlertDialog.Builder(taskthreeScreen);
        builderSingle.setTitle(R.string.SelectDOB);

        ArrayList<String> years = new ArrayList<String>();
        int thisYear = Calendar.getInstance().get(Calendar.YEAR);
        for (int i = 1940; i <= thisYear; i++) {
            years.add(Integer.toString(i));
        }
        final ArrayAdapter<String> adapter = new ArrayAdapter<String>(taskthreeScreen, android.R.layout.select_dialog_singlechoice);
        adapter.addAll(years);

        builderSingle.setNegativeButton("cancel", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
            }
        });

        builderSingle.setAdapter(adapter, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dobEditText.setText(adapter.getItem(which).toString());

            }
        });
        builderSingle.show();
    }
}
