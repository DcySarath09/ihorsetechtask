package com.ihorsetech.task.ui.tasktwo;

import android.annotation.TargetApi;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Build;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.os.Handler;
import android.view.View;
import android.widget.Toast;

import androidx.appcompat.app.AlertDialog;
import androidx.databinding.DataBindingUtil;

import com.ihorsetech.task.R;
import com.ihorsetech.task.base.BaseActivity;
import com.ihorsetech.task.databinding.TasktwoScreenBinding;
import com.ihorsetech.task.ui.tasktwo.location.JobService;
import com.ihorsetech.task.ui.tasktwo.location.Service;
import com.ihorsetech.task.uitls.Preference;

import java.util.ArrayList;

import static android.Manifest.permission.ACCESS_COARSE_LOCATION;
import static android.Manifest.permission.ACCESS_FINE_LOCATION;

public class TaskTwoScreen extends BaseActivity implements View.OnClickListener{

    /*View binding*/
    TasktwoScreenBinding binding;
    Preference preference;

    /*Create Runtime Permission Objects*/
    private ArrayList permissionsToRequest;
    private ArrayList permissionsRejected = new ArrayList();
    private ArrayList permissions = new ArrayList();
    private final static int ALL_PERMISSIONS_RESULT = 101;

    /*Service class object*/

    /*Handler objects**/
    Handler handler = new Handler();
    Runnable runnable;
    int delay = 30000;
    CountDownTimer locationCountDownTimer;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        initView();

        /*Object Initialization*/
        preference = Preference.getInstance(this);
        if(preference.isLoggedin()){
            binding.logoutBtn.setVisibility(View.VISIBLE);
            binding.loginBtn.setVisibility(View.GONE);
            binding.latitudeTxt.setVisibility(View.VISIBLE);
            binding.longituteTxt.setVisibility(View.VISIBLE);
            binding.timmerTxt.setVisibility(View.VISIBLE);
            startTimer();
        }else{
            binding.logoutBtn.setVisibility(View.GONE);
            binding.loginBtn.setVisibility(View.VISIBLE);
            binding.latitudeTxt.setVisibility(View.GONE);
            binding.longituteTxt.setVisibility(View.GONE);
            binding.timmerTxt.setVisibility(View.GONE);
        }

        binding.loginBtn.setOnClickListener(this);
        binding.logoutBtn.setOnClickListener(this);

        /*Permission Check*/
        permissions.add(ACCESS_FINE_LOCATION);
        permissions.add(ACCESS_COARSE_LOCATION);
        permissionsToRequest = findUnAskedPermissions(permissions);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (permissionsToRequest.size() > 0)
                requestPermissions((String[]) permissionsToRequest.toArray(new String[permissionsToRequest.size()]), ALL_PERMISSIONS_RESULT);
        }
    }

    private void initView() {
        binding = DataBindingUtil.setContentView(this, R.layout.tasktwo_screen);
        showBackArrow();
        setTitle("Task-2");
    }


    @Override
    public void onClick(View view) {

        switch (view.getId()){

            case R.id.loginBtn:
                preference.saveLogin(true);
                toast("Login Successfully");
                binding.logoutBtn.setVisibility(View.VISIBLE);
                binding.loginBtn.setVisibility(View.GONE);
                startService(new Intent(TaskTwoScreen.this, Service.class));
                toast("Location update start");
                binding.longituteTxt.setVisibility(View.VISIBLE);
                binding.latitudeTxt.setVisibility(View.VISIBLE);
                binding.timmerTxt.setVisibility(View.VISIBLE);
                startTimer();
                startHandelMethod();
                break;

            case R.id.logoutBtn:
                preference.saveLogin(false);
                toast("Logout Successfully");
                binding.logoutBtn.setVisibility(View.GONE);
                binding.loginBtn.setVisibility(View.VISIBLE);
                stopService(new Intent(TaskTwoScreen.this, Service.class));
                JobService.stopJob(TaskTwoScreen.this);
                toast("Location update stop");
                handler.removeCallbacks(runnable);
                locationCountDownTimer.cancel();
                binding.longituteTxt.setVisibility(View.GONE);
                binding.latitudeTxt.setVisibility(View.GONE);
                binding.timmerTxt.setVisibility(View.GONE);
                binding.latitudeTxt.setText("Loading...");
                binding.longituteTxt.setText("Loading...");

                break;
        }
    }

    private ArrayList findUnAskedPermissions(ArrayList wanted) {
        ArrayList result = new ArrayList();
        for (int i=0;i<wanted.size();i++){
            if (!hasPermission((String) wanted.get(i))) {
                result.add(wanted.get(i));
            }
        }
        return result;
    }

    private boolean hasPermission(String permission) {
        if (canMakeSmores()) {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                return (checkSelfPermission(permission) == PackageManager.PERMISSION_GRANTED);
            }
        }
        return true;
    }

    private boolean canMakeSmores() {
        return (Build.VERSION.SDK_INT > Build.VERSION_CODES.LOLLIPOP_MR1);
    }


    @Override
    protected void onResume() {
        if(preference.isLoggedin()){
            startHandelMethod();
        }
        super.onResume();
    }

    private void startHandelMethod() {
        handler.postDelayed(runnable = new Runnable() {
            public void run() {
                handler.postDelayed(runnable, delay);
                if(Service.getmCurrentService()!=null){
                    if(Service.getmCurrentService().providerStatus().equalsIgnoreCase("ON") && Service.getmCurrentService().getLongitude()>0 && Service.getmCurrentService().getLatitude()>0){
                        binding.latitudeTxt.setText("Latitude:"+Service.getmCurrentService().getLatitude());
                        binding.longituteTxt.setText("Longitude:"+Service.getmCurrentService().getLongitude());
                    }else{
                        binding.latitudeTxt.setText("GPS Service "+Service.getmCurrentService().providerStatus());
                        binding.longituteTxt.setText("GPS Service "+Service.getmCurrentService().providerStatus());
                    }
                }
                startTimer();
                toast("Location got Updated!!");
            }
        }, delay);
    }

    /*Timmer show*/
    private void startTimer() {
        locationCountDownTimer = new CountDownTimer(30000, 1000){
            public void onTick(long millisUntilFinished){
                binding.timmerTxt.setText(""+millisUntilFinished / 1000);
            }
            public  void onFinish(){
                binding.timmerTxt.setText("Location got Updated!!");
            }
        }.start();
    }

    @TargetApi(Build.VERSION_CODES.M)
    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {

        switch (requestCode) {

            case ALL_PERMISSIONS_RESULT:
                for (int i=0;i<permissionsToRequest.size();i++){
                    if (!hasPermission((String) permissionsToRequest.get(i))) {
                        permissionsRejected.add(permissionsToRequest.get(i));
                    }
                }
                if (permissionsRejected.size() > 0) {
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                        if (shouldShowRequestPermissionRationale((String) permissionsRejected.get(0))) {
                            showMessageOKCancel("These permissions are mandatory for the application. Please allow access.",
                                    new DialogInterface.OnClickListener() {
                                        @Override
                                        public void onClick(DialogInterface dialog, int which) {
                                            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                                                requestPermissions((String[]) permissionsRejected.toArray(new String[permissionsRejected.size()]), ALL_PERMISSIONS_RESULT);
                                            }
                                        }
                                    });
                            return;
                        }
                    }
                }
                break;
        }

    }

    private void showMessageOKCancel(String message, DialogInterface.OnClickListener okListener) {
        new AlertDialog.Builder(TaskTwoScreen.this)
                .setMessage(message)
                .setPositiveButton("OK", okListener)
                .setNegativeButton("Cancel", null)
                .create()
                .show();
    }


    @Override
    protected void onPause() {
        super.onPause();
        handler.removeCallbacks(runnable);
    }
}
