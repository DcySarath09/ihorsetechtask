package com.ihorsetech.task.ui.taskone;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;

import androidx.databinding.DataBindingUtil;

import com.ihorsetech.task.R;
import com.ihorsetech.task.base.BaseActivity;
import com.ihorsetech.task.databinding.TaskoneScreenBinding;

public class TaskOneScreen extends BaseActivity implements View.OnClickListener {

    TaskoneScreenBinding binding;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        initView();

        //set click event
        binding.currentPriceBtn.setOnClickListener(this);
        binding.historicalBtn.setOnClickListener(this);
    }

    private void initView() {
        binding = DataBindingUtil.setContentView(this, R.layout.taskone_screen);
        showBackArrow();
        setTitle("Task-1");
    }

    @Override
    public void onClick(View view) {

        switch (view.getId()){

            case R.id.currentPriceBtn:
                startActivity(new Intent(TaskOneScreen.this, CurrencyRateScreen.class));
                break;

            case R.id.historicalBtn:
                startActivity(new Intent(TaskOneScreen.this, DatePriceListScreen.class));
                break;
        }
    }
}
