package com.ihorsetech.task.ui.taskone;

import java.io.Serializable;

public class DatePriceModel implements Serializable {

    String data;
    String amount;

    public String getData() {
        return data;
    }

    public void setData(String data) {
        this.data = data;
    }

    public String getAmount() {
        return amount;
    }

    public void setAmount(String amount) {
        this.amount = amount;
    }
}
