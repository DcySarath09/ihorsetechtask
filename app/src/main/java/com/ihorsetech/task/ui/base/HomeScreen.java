package com.ihorsetech.task.ui.base;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;

import androidx.databinding.DataBindingUtil;

import com.ihorsetech.task.R;
import com.ihorsetech.task.base.BaseActivity;
import com.ihorsetech.task.databinding.HomeScreenBinding;
import com.ihorsetech.task.ui.taskone.TaskOneScreen;
import com.ihorsetech.task.ui.taskthree.TaskthreeScreen;
import com.ihorsetech.task.ui.tasktwo.TaskTwoScreen;

public class HomeScreen extends BaseActivity implements View.OnClickListener {

    HomeScreenBinding binding;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        initView();

        //set click event
        binding.taskOneBtn.setOnClickListener(this);
        binding.taskTwoBtn.setOnClickListener(this);
        binding.taskThreeBtn.setOnClickListener(this);
    }

    private void initView() {
        binding = DataBindingUtil.setContentView(this, R.layout.home_screen);
        hideActionBar();
        transparentToolbar();
    }

    @Override
    public void onClick(View view) {

        switch (view.getId()){

            case R.id.taskOneBtn:
                startActivity(new Intent(HomeScreen.this, TaskOneScreen.class));
                break;
            case R.id.taskTwoBtn:
                startActivity(new Intent(HomeScreen.this, TaskTwoScreen.class));
                break;
            case R.id.taskThreeBtn:
                startActivity(new Intent(HomeScreen.this, TaskthreeScreen.class));
                break;
        }
    }
}
