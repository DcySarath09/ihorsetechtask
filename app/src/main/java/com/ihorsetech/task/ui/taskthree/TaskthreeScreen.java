package com.ihorsetech.task.ui.taskthree;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.TextView;

import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.ihorsetech.task.R;
import com.ihorsetech.task.base.BaseActivity;
import com.ihorsetech.task.databinding.TaskthreeScreenBinding;

import java.util.ArrayList;
import java.util.Calendar;

public class TaskthreeScreen extends BaseActivity implements View.OnClickListener, CompoundButton.OnCheckedChangeListener {

    //Create objects
    TaskthreeScreenBinding binding;
    private UserListAdapter userListAdapter;

    //Create global arraylist
    ArrayList<UserModel> userModelArrayList = new ArrayList<>();

    //Create dialogue object
    Dialog addUserDialogue;
    String selectedTab = "MALE";

    int addedPosition = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        initView();
        updateTaskList();
    }

    /****View Binding****/
    private void initView() {
        binding = DataBindingUtil.setContentView(this, R.layout.taskthree_screen);
        showBackArrow();
        setTitle("Task-3");

        //setClick Event
        binding.addButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                addUser();
            }
        });
        binding.maleTxt.setOnClickListener(this);
        binding.femaleTxt.setOnClickListener(this);
        binding.verticalBtn.setOnCheckedChangeListener(this);
        binding.horizontalBtn.setOnCheckedChangeListener(this);
    }

    /*Form RecyclerView*/
    private void updateTaskList() {
        if(userModelArrayList.size()>0){
            binding.userRecyclerView.setVisibility(View.VISIBLE);
            binding.tabBtn.setVisibility(View.VISIBLE);
            binding.tabIndicator.setVisibility(View.VISIBLE);
            binding.radioGroup.setVisibility(View.VISIBLE);
            initRecyclerView();
        }else{
            binding.userRecyclerView.setVisibility(View.GONE);
            binding.tabBtn.setVisibility(View.GONE);
            binding.tabIndicator.setVisibility(View.GONE);
            binding.radioGroup.setVisibility(View.GONE);
        }
    }

    /*Add user dialogue*/
    private void addUser() {
        if (addUserDialogue == null) {
            addUserDialogue = new Dialog(TaskthreeScreen.this);
            addUserDialogue.requestWindowFeature(Window.FEATURE_NO_TITLE);
            addUserDialogue.setContentView(R.layout.dialog_for_adduser);
            addUserDialogue.getWindow().setBackgroundDrawable(new ColorDrawable(Color.WHITE));
            addUserDialogue.getWindow().setLayout(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.MATCH_PARENT);
        }

        final TextView titleTxt = (TextView)addUserDialogue.findViewById(R.id.titleTxt);
        final EditText nameEditText     = (EditText)addUserDialogue.findViewById(R.id.nameEditText);
        final EditText lastNameEditText = (EditText)addUserDialogue.findViewById(R.id.lastNameEditText);
        final EditText dobEditText        = (EditText)addUserDialogue.findViewById(R.id.dobEditText);
        final RadioButton maleBtn       = (RadioButton)addUserDialogue.findViewById(R.id.maleBtn);
        RadioButton femaleBtn           = (RadioButton)addUserDialogue.findViewById(R.id.femaleBtn);
        Button submitBtn                = (Button)addUserDialogue.findViewById(R.id.submitBtn);

        titleTxt.setText(R.string.AddUser);
        //setSpinner values
        dobEditText.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                getDoBfromPopUp(dobEditText);
            }
        });

        submitBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                String fName = nameEditText.getText().toString().trim();
                String lName = lastNameEditText.getText().toString().trim();
                String dob = dobEditText.getText().toString();

                if(fName.isEmpty()){
                    nameEditText.setError(getResources().getString(R.string.EnterFirstName));
                }else if(lName.isEmpty()){
                    lastNameEditText.setError(getResources().getString(R.string.EnterLastName));
                }else{
                    UserModel userModel = new UserModel();
                    userModel.setFirstName(fName);
                    userModel.setLastName(lName);
                    userModel.setDob(dob);

                    if(maleBtn.isChecked()){
                        userModel.setGender("Male");
                    }else{
                        userModel.setGender("Female");
                    }
                    userModelArrayList.add(userModel);
                    toast(getResources().getString(R.string.UserAdded));
                    nameEditText.setText("");
                    lastNameEditText.setText("");
                    addUserDialogue.dismiss();
                    updateTaskList();
                }
            }
        });

        addUserDialogue.setCancelable(true);
        addUserDialogue.show();

    }

    /*Delete Function*/
    public void callDelete(int position){
        userModelArrayList.remove(position);
        updateTaskList();
        toast(getResources().getString(R.string.UserDeleted));
    }

    /*Update Function*/
    public void updateUser(int position, UserModel userModel){
        userModelArrayList.set(position,userModel);
        updateTaskList();
        toast(getResources().getString(R.string.UseUpdated));
    }

    /*Click event*/
    @Override
    public void onClick(View view) {

        switch (view.getId()){

            case R.id.maleTxt:
                selectedTab = "MALE";
                binding.maleIndi.setVisibility(View.VISIBLE);
                binding.femaleIndi.setVisibility(View.INVISIBLE);
                initRecyclerView();
                break;

            case R.id.femaleTxt:
                selectedTab = "FEMALE";
                binding.maleIndi.setVisibility(View.INVISIBLE);
                binding.femaleIndi.setVisibility(View.VISIBLE);
                initRecyclerView();
                break;
        }
    }

    /*Setup RecyclerView*/
    public void initRecyclerView(){
        if(binding.verticalBtn.isChecked()){
            binding.userRecyclerView.setLayoutManager(new LinearLayoutManager(this, RecyclerView.VERTICAL,false));
        }else{
            binding.userRecyclerView.setLayoutManager(new LinearLayoutManager(this, RecyclerView.HORIZONTAL,false));
        }
        userListAdapter = new UserListAdapter(userModelArrayList,this,selectedTab);
        binding.userRecyclerView.setAdapter(userListAdapter);
        userListAdapter.notifyDataSetChanged();
    }

    /*Orientation setup*/
    @Override
    public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
        switch (compoundButton.getId()){
            case R.id.verticalBtn:
                initRecyclerView();
                break;
            case R.id.horizontalBtn:
                initRecyclerView();
                break;
        }
    }

    /*DOB PopUp*/
    private void getDoBfromPopUp(final EditText dobEditText) {

        final String returnValue = "";
        AlertDialog.Builder builderSingle = new AlertDialog.Builder(TaskthreeScreen.this);
        builderSingle.setTitle(R.string.SelectDOB);

        ArrayList<String> years = new ArrayList<String>();
        int thisYear = Calendar.getInstance().get(Calendar.YEAR);
        for (int i = 1940; i <= thisYear; i++) {
            years.add(Integer.toString(i));
        }
        final ArrayAdapter<String> adapter = new ArrayAdapter<String>(this, android.R.layout.select_dialog_singlechoice);
        adapter.addAll(years);

        builderSingle.setNegativeButton("cancel", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
            }
        });

        builderSingle.setAdapter(adapter, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dobEditText.setText(adapter.getItem(which).toString());

            }
        });
        builderSingle.show();
    }
}
