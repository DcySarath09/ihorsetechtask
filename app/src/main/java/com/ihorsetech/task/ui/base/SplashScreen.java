package com.ihorsetech.task.ui.base;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;

import com.ihorsetech.task.R;
import com.ihorsetech.task.base.BaseActivity;


public class SplashScreen extends BaseActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.splash_screen);

        //Hide actionbar & status bar
        hideActionBar();
        transparentToolbar();
        lunchHomeScreen();
    }

    /*Open Main Screen*/
    private void lunchHomeScreen() {
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                startActivity(new Intent(SplashScreen.this,HomeScreen.class)
                        .setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK));
                finish();
            }
        },2000);
    }
}
