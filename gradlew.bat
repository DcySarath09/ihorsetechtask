package com.foloosi.customer;

import android.util.SparseArray;
import android.util.SparseIntArray;
import android.view.View;
import androidx.databinding.DataBinderMapper;
import androidx.databinding.DataBindingComponent;
import androidx.databinding.ViewDataBinding;
import com.foloosi.customer.databinding.AddNewCardBindingImpl;
import com.foloosi.customer.databinding.CouponScreenBindingImpl;
import com.foloosi.customer.databinding.SavedCardActBindingImpl;
import java.lang.IllegalArgumentException;
import java.lang.Integer;
import java.lang.Object;
import java.lang.Override;
import java.lang.RuntimeException;
import java.lang.String;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class DataBinderMapperImpl extends DataBinderMapper {
  private static final int LAYOUT_ADDNEWCARD = 1;

  private static final int LAYOUT_COUPONSCREEN = 2;

  private static final int LAYOUT_SAVEDCARDACT = 3;

  private static final SparseIntArray INTERNAL_LAYOUT_ID_LOOKUP = new SparseIntArray(3);

  static {
    INTERNAL_LAYOUT_ID_LOOKUP.put(com.foloosi.customer.R.layout.add_new_card, LAYOUT_ADDNEWCARD);
    INTERNAL_LAYOUT_ID_LOOKUP.put(com.foloosi.customer.R.layout.coupon_screen, LAYOUT_COUPONSCREEN);
    INTERNAL_LAYOUT_ID_LOOKUP.put(com.foloosi.customer.R.layout.saved_card_act, LAYOUT_SAVEDCARDACT);
  }

  @Override
  public ViewDataBinding getDataBinder(DataBindingComponent component, View view, int layoutId) {
    int localizedLayoutId = INTERNAL_LAYOUT_ID_LOOKUP.get(layoutId);
    if(localizedLayoutId > 0) {
      final Object tag = view.getTag();
      if(tag == null) {
        throw new RuntimeException("view must have a tag");
      }
      switch(localizedLayoutId) {
        case  LAYOUT_ADDNEWCARD: {
          if ("layout/add_new_card_0".equals(tag)) {
            return new AddNewCardBindingImpl(component, view);
          }
          throw new IllegalArgumentException("The tag for add_new_card is invalid. Received: " + tag);
        }
        case  LAYOUT_COUPONSCREEN: {
          if ("layout/coupon_screen_0".equals(tag)) {
            return new CouponScreenBindingImpl(component, view);
          }
          throw new IllegalArgumentException("The tag for coupon_screen is invalid. Received: "